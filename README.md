Temple Rage is small project in unity3d engine and c#. While creating, I was inspired by games like the binding of isaac and Paranautical Activity.
In the final product, I'd like to add a kind of procedural dungeon generation that will have a shop, treasure, boss, and 3-5 normal rooms.
Various items, 15-30 special ability and 3-5 special enemys(boss).Now in prototype build I implemented some mechanic.

-Audio manager.
-Health/mana logic.
-Two Enemys- Skeleton and pigman.
-Mana orb showing after kills enemy.
-Basic attack- Magic beam, use it if you dont have mana.
-Fireball- Strong spell.
-Display damage done and crit hits.
-Duble jump.
-Small map for testing.

![ImgName](https://cdn.discordapp.com/attachments/296316217893978113/836249300975091762/unknown.png)
![ImgName](https://cdn.discordapp.com/attachments/296316217893978113/836249701628116992/unknown.png)
![ImgName](https://cdn.discordapp.com/attachments/296316217893978113/836250006335782922/unknown.png)
![ImgName](https://cdn.discordapp.com/attachments/296316217893978113/836250510046003220/unknown.png)


